var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var requests = require(__dirname + '/requests.js');

var files = bookshelf.Model.extend({
  tableName: 'Files',
  class: function() {
    return this.belongsTo(requests, 'requestID');
  }
});

module.exports = files;
