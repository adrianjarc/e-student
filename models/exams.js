var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var classes = require(__dirname + '/classes.js');

var exams = bookshelf.Model.extend({
  tableName: 'Exams',
  class: function() {
    return this.belongsTo(classes, 'classID');
  }
});

module.exports = exams;
