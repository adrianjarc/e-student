var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var user = require(__dirname + '/user.js');
var files = require(__dirname + '/files.js');

var request = bookshelf.Model.extend({
  tableName: 'Requests',
  idAttribute: 'requestID',
  user: function() {
    return this.belongsToMany(user, 'StudentRequests', 'requestID', 'userID').withPivot(['status', 'additionalComments']);
  },
  files: function() {
    return this.hasMany(files, 'requestID');
  }
});

module.exports = request;
