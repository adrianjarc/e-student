var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var users = require(__dirname + '/user.js');

var order = bookshelf.Model.extend({
  tableName: 'Orders',
  idAttribute: 'orderID',
  user: function() {
    return this.belongsToMany(users, 'StudentOrders', 'orderID', 'userID'). withPivot(['quantity', 'language', 'pickup']);
  }
});

module.exports = order;
