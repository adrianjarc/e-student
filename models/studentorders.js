var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var orders = require(__dirname + '/orders.js');
var users   = require(__dirname + '/user.js')

var studOrders = bookshelf.Model.extend({
  tableName: 'StudentOrders',
  orders: function() {
    return this.hasMany(orders);
  },
  users: function () {
    return this.hasMany(users);
  }
});

module.exports = studOrders;
