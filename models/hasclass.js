var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var classes = require(__dirname + '/classes.js');
var users   = require(__dirname + '/user.js')

var hasClass = bookshelf.Model.extend({
  tableName: 'HasClass',
  classes: function() {
    return this.hasMany(classes);
  },
  users: function () {
    return this.hasMany(users);
  }
});

module.exports = hasClass;
