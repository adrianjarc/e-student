var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var requests = require(__dirname + '/requests.js');
var users   = require(__dirname + '/user.js')

var studRequests = bookshelf.Model.extend({
  tableName: 'StudentRequests',
  requests: function() {
    return this.hasMany(requests);
  },
  users: function () {
    return this.hasMany(users);
  }
});

module.exports = studRequests;
