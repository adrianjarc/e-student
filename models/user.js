var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var classes = require(__dirname + '/classes.js');
var orders = require(__dirname + '/orders.js');
var requests = require(__dirname + '/requests.js');

var user = bookshelf.Model.extend({
  tableName: 'Users',
  idAttribute: 'userID',
  class: function() {
    return this.belongsToMany(classes, 'HasClass', 'userID', 'classID').withPivot(['mark']);
  },
  order: function() {
    return this.belongsToMany(orders, 'StudentOrders', 'userID', 'orderID'). withPivot(['quantity', 'language', 'pickup']);
  },
  request: function() {
    return this.belongsToMany(requests, 'StudentRequests', 'userID', 'requestID'). withPivot(['status', 'additionalComments']);
  }
});

module.exports = user;
