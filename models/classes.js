var knexconf = require(__dirname + '/../config.json').knex;

var knex      = require('knex')(knexconf),
    bookshelf = require('bookshelf')(knex);

var exams = require(__dirname + '/exams.js');
var user = require(__dirname + '/user.js');

var classes = bookshelf.Model.extend({
  tableName: 'Classes',
  idAttribute: 'classID',
  exams: function() {
    return this.hasMany(exams, 'classID');
  },
  user: function () {
    return this.belongsToMany(user, 'HasClass', 'classID', 'userID').withPivot(['mark']);
  }
});

module.exports = classes;
