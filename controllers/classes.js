var moment  = require('moment');

/**
 * @api {post} /class/add Add class
 * @apiName Add
 * @apiGroup Class
 *
 * @apiDescription Add new class.
 * This call can only be made by user whose role is referat.
 *
 * @apiParam {String} className Name of class
 * @apiParam {Number} [profId] Id of professor teaching this class
 */
exports.add = function (req, res) {

  // Check if user is allowed to preform this action
  if (req.user.role !== 'referat') {
    return res.status(403).send({error: 'Your role must be referat'});
  }

  var className = req.body.className;
  var profId = req.body.profId;

  // If there is no class name return error
  if (className === undefined || className === '') {
    console.error(moment().format(req.format), 'ERROR:', 'Class name should not be empty.');
    return res.status(400).json({error: 'Class name should not be empty.'});
  }
  // Otherwise add class
  else {
    new req.db.classes({
      'name': className
    })
    .save()
    .then(function(cls) {
      if (cls) {
        // If professor teaching this class is already added add connection
        if (profId) {
          new req.db.hascls({
            'userID': profId,
            'classID': cls.get('classID')
          })
          .save()
          .then(function(hascls) {
            console.log(moment().format(req.format), 'SUCCESS:', 'Class successfully added.');
            console.log(moment().format(req.format), 'SUCCESS:', 'Connection successfully added:', hascls.toJSON());
            return res.send({status: 'Class successfully added.'});
          });
        }
        // Otherwise just respond that class was added
        else {
          console.log(moment().format(req.format), 'SUCCESS:', 'Class successfully added.');
          return res.send({status: 'Class successfully added.'});
        }
      }
    })
    .catch(function(err) {
      console.error(moment().format(req.format), 'ERROR:', err);
      return res.status(500).json({error: 'Server error'});
    });
  }
}
