var jwt     = require('jsonwebtoken'),
    moment  = require('moment'),
    crypto  = require('crypto');

var config = require(__dirname + '/../config.json');


/**
 * @api {post} /login Login
 * @apiName Login
 * @apiGroup Login
 *
 * @apiDescription Login to api. Need this step first to aquire token.
 * This method logs you in to api and sends you back a token with which you can than send requests to api.
 *
 * @apiParam {String} username Username
 * @apiParam {String{8..}} password Password
 */
exports.login = function (req, res) {
  req.db.users.where('username', req.body.username).fetch(['userID','password']).then(function(usr) {
    if (usr) {

      // Decrypt password in database
      var decipher = crypto.createDecipher('aes192', config.secret);
      var decrypted = decipher.update(usr.get('password'), 'hex', 'utf8');
      decrypted += decipher.final('utf8');

      // If password in DB and recieved password match log user in.
      if (decrypted === req.body.password) {

        // Create token so we can identify user at anytime
        // if there is no token user is automaticallz redirected to login
        var token = jwt.sign({
          id: usr.get('userID'),
          role: usr.get('role')
        }, config.secret);
        res.status(200);
        res.cookie('token', token, { secure:true, maxAge: 10800*1000 });
        console.log(moment().format('D.M.YYYY HH:mm:'), 'SUCCESS:', 'User authenticated.');
        return res.json({status: 'OK'});
      } else {
        console.error(moment().format('D.M.YYYY HH:mm:'), 'ERROR:', 'Wrong password');
        return res.status(401).json({error: 'Wrong password'});
      }
    } else {
      console.error(moment().format('D.M.YYYY HH:mm:'), 'ERROR:', 'Wrong username');
      return res.status(401).json({error: 'Wrong username'});
    }
  }).catch(function(err) {
    console.error(moment().format('D.M.YYYY HH:mm:'), 'ERROR:', err);
    return res.status(500).json({error: 'Server error'});
  });
};
