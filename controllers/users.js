var moment  = require('moment'),
    crypto  = require('crypto');

var config = require(__dirname + '/../config.json');


/**
 * @api {post} /user/changepwd Chage user password
 * @apiName ChangePwd
 * @apiGroup User
 *
 * @apiDescription Change your user password.
 *
 * @apiParam {String{8..}} pass1 Wanted password
 * @apiParam {String{8..}} pass2 Confirm wanted password
 */
exports.changepwd = function (req, res) {
  // Check if password is longer than 8 characters
  if (req.body.pass1.length < 8) {
    return res.status(400).json({error: 'Password too short'});
  }
  // If passwords match change password
  else if (req.body.pass1 === req.body.pass2) {

    // Encrypt password before it is saved to database
    var cipher = crypto.createCipher('aes192', config.secret);
    var crypted = cipher.update(req.body.pass1, 'utf8', 'hex');
    crypted += cipher.final('hex');

    // Save new password
    new req.db.users({
      'userID': req.user.id,
      'password': crypted
    })
    .save()
    .then(function(usr) {
      if (usr) {
        console.log(moment().format(req.format), 'SUCCESS:', 'Password changed');
        return res.send({status: 'Password changed succesfully'})
      }
    })
    .catch(function(err) {
      console.error(moment().format(req.format), 'ERROR:', err);
      return res.status(500).json({error: 'Server error'});
    });
  }
  // Else return error
  else {
    return res.status(400).json({error: 'Passwords do not match'});
  }
}

/**
 * @api {post} /user/add Add user
 * @apiName Add
 * @apiGroup User
 *
 * @apiDescription Add new user.
 * This call can only be made by user whose role is referat.
 *
 * @apiParam {String} firstName User's first name
 * @apiParam {String} lastName User's last name
 * @apiParam {String="student","professor","referat"} role User's role
 * @apiParam {String} street Street on which user lives
 * @apiParam {Number} post Post of the city user lives in
 * @apiParam {String} city City in which user lives
 */
exports.add = function (req, res) {

  if (req.user.role !== 'referat') {
    return res.status(403).send({error: 'You must be referat'});
  }

  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var role = req.body.role;
  var street = req.body.street;
  var post = req.body.post;
  var city = req.body.city;
  var errors = [];


  // First name can have at most 2 words both must begin with capital letter
  // Same goes for last name
  var nameRegex = new RegExp('^[A-ZČŠŽĆĐ][a-yčšžćđ]+( [A-ZČŠŽĆĐ][a-yčšžćđ]+)?');
  var fnRegMatch = firstName.match(nameRegex);

  if (firstName === undefined || firstName === '') {
    errors.push('Name should not be empty.');
  } else if (!fnRegMatch) {
    errors.push('First name can consist of 2 names and both must begin with capital letter.');
  }

  var lnRegMatch = lastName.match(nameRegex);

  if (lastName === undefined || lastName === '') {
    errors.push('Last name should not be empty.');
  } else if (!lnRegMatch) {
    errors.push('Last name can consist of 2 names and both must begin with capital letter.');
  }

  if (role === undefined || role === '') {
    errors.push('Role should not be empty.');
  }

  var streetRegex = new RegExp('^[A-ZČŠŽĆĐ][a-zčšžćđ]+( [A-ZČŠŽĆĐ]?[a-zčšžćđ]+)* \\d{1,3}');
  var stRegMatch = street.match(streetRegex);

  if (street === undefined || street === '') {
    errors.push('Street should not be empty.');
  } else if (!stRegMatch) {
    errors.push('Street must include atleast one word begginig with capital letter and between 1 and 3 digits.');
  }

  var postRegex = new RegExp('[1-9]\\d{3,6}');
  var poRegMatch = post.match(postRegex);

  if (post === undefined || post === '') {
    errors.push('Post should not be empty.');
  } else if (!poRegMatch) {
    errors.push('Post must have atleast four digits and it must begin with 1.');
  }

  var cityRegex = new RegExp('^[A-ZČŠŽĆĐ][a-yčšžćđ]+( [A-ZČŠŽĆĐ][a-yčšžćđ]+)?');
  var ciRegMatch = city.match(cityRegex);

  if (city === undefined || city === '') {
    errors.push('City should not be empty.');
  } else if (!ciRegMatch) {
    errors.push('City can be two words (must be atleast one). Both words must begin with capital letter.');
  }

  if (errors.length > 0) {
    console.error(moment().format(req.format), 'ERROR:', errors.join('\n'));
    return res.status(400).json({error: errors.join('<br />')});
  } else {
    var username = firstName.toLowerCase() + lastName.toLowerCase().charAt(0);
    var password;

    switch (role) {
      case 'student':
        password = firstName.toLowerCase() + '1234';
      break;
      case 'professor':
        password = lastName.toLowerCase() + '4321';
      break;
      case 'referat':
        password = username + '12';
      break;
      default:
        password = firstName.toLowerCase() + '1234';
    }

    var cipher = crypto.createCipher('aes192', config.secret);
    var crypted = cipher.update(password, 'utf8', 'hex');
    crypted += cipher.final('hex');

    var wildcard = username + '%';

    req.db.users.where('username', 'like', wildcard)
    .orderBy('userID', 'DESC')
    .fetchAll()
    .then(function (usrs) {
      var usersArr = usrs.toJSON();

      if (usersArr.length > 0) {
        var num = parseInt(usersArr[0].username.replace(username, ''));

        if (!num) {
          num = 1;
        } else {
          num += 1;
        }

        username += num;

      }
      // Add user
      new req.db.users({
        'firstName': firstName,
        'lastName': lastName,
        'username': username,
        'password': crypted,
        'role': role,
        'street': street,
        'post': parseInt(post),
        'city': city
      })
      .save()
      .then(function (usr) {
        if (usr) {
          console.log(moment().format(req.format), 'SUCCESS:', 'User added');
          if (role === 'professor') {
            return res.send({
              status: 'User succesfully added.',
              user: usr.toJSON()
            });
          } else {
            return res.send({status: 'User succesfully added.'});
          }
        }
      })
      .catch(function(err) {
        console.error(moment().format(req.format), 'ERROR:', err);
        return res.status(500).json({error: 'Server error'});
      });

    })
    .catch(function(err) {
      console.error(moment().format(req.format), 'ERROR:', err);
      return res.status(500).json({error: 'Server error'});
    });
  }
}
