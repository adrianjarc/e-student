var moment  = require('moment');

/**
 * @api {get} /changepwd Load change password page
 * @apiName ChangePwd
 * @apiGroup Pages
 *
 * @apiDescription Loads change password page.
 */
exports.change = function (req, res) {
  req.db.users.where({'userID': req.user.id})
  .fetch({
    require: true
  })
  .then(function(usr) {
    if (usr) {
      console.log(moment().format(req.format), 'SUCCESS:', 'Load change password page.');
      var role = usr.get('role');
      return res.render('changepwd', {
          user: {
            firstName: usr.get('firstName'),
            lastName: usr.get('lastName'),
            student: role === 'student',
            professor:  role === 'professor',
            referat: role === 'referat'
          }
      });
    }
  }).catch(function(err) {
    console.error(moment().format(req.format), 'ERROR:', err);
    return res.status(500).json({error: 'Server error'});
  });
}


/**
 * @api {get} /home Load home page
 * @apiName Home
 * @apiGroup Pages
 *
 * @apiDescription Loads home page.
 */
exports.home = function (req, res) {
  req.db.users.where({'userID': req.user.id})
  .fetch({
    require: true,
    withRelated: [{ class: function(query) { query.orderBy('classID'); }}]
  })
  .then(function(usr) {
    if (usr) {
      var render;
      switch (usr.get('role')) {
        case 'student':
          render = 'homes';
        break;
        case 'professor':
          render = 'homep';
        break;
        case 'referat':
          render = 'homer';
        break;
        default:
          render = 'homes';
      }
      var role = usr.get('role');
      console.log(moment().format(req.format), 'SUCCESS:', 'Load home page.');
      if (render === 'homer') {
        req.db.users.where('role', 'professor')
        .orderBy('firstName', 'ASC')
        .fetchAll()
        .then(function(profs) {
          if (profs) {
            return res.render(render, {
                user: {
                  firstName: usr.get('firstName'),
                  lastName: usr.get('lastName'),
                  professors: profs.toJSON(),
                  student: role === 'student',
                  professor:  role === 'professor',
                  referat: role === 'referat'
                }
            });
          }
        });
      } else {
        return res.render(render, {
          user: {
            firstName: usr.get('firstName'),
            lastName: usr.get('lastName'),
            classes: usr.related('class').toJSON(),
            student: role === 'student',
            professor:  role === 'professor',
            referat: role === 'referat'
          }
        });
      }
    }
  }).catch(function(err) {
    console.error(moment().format(req.format), 'ERROR:', err);
    return res.status(500).json({error: 'Server error'});
  });
}
