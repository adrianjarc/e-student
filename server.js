// Require Modules
var fs            = require('fs'),
    express       = require('express'),
    http2         = require('http2'),
    hbl           = require('express-handlebars'),
    http          = require('http'),
    moment        = require('moment'),
    bodyParser    = require('body-parser'),
    cookieParser  = require('cookie-parser'),
    exec          = require('child_process').exec;

// Load middlewares
var middlewares = {
  'jwtcheck': require(__dirname + '/middlewares/token.js'),
  'loadDB'  : require(__dirname + '/middlewares/loadDB.js')
};

// Load config
var config = require(__dirname + '/config.json');

// Load controllers
var pages = require(__dirname + '/controllers/pages.js');
var login = require(__dirname + '/controllers/login.js');
var user = require(__dirname + '/controllers/users.js');
var classes = require(__dirname + '/controllers/classes.js');

// Create Express Application
var app = express();

// Make HTTP2 work with Express (this must be before any other middleware)
require('express-http2-workaround')({ express:express, http2:http2, app:app });

app.engine('.hbs', hbl({extname: '.hbs', defaultLayout: 'main'}));
app.set('view engine', '.hbs');
app.use(express.static('public'));
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());
app.use(bodyParser.text({
  type: 'text/*'
}));
app.use(cookieParser());

// Setup HTTP/2 Server
var httpsOptions = {
    'key' : fs.readFileSync(__dirname + config.cert.key),
    'cert' : fs.readFileSync(__dirname + config.cert.cert),
    'ca' : fs.readFileSync(__dirname + config.cert.ca)
};

var format = 'D.M.YYYY HH:mm:';


// Create server
var server = http2.createServer(httpsOptions, app);

server.listen(config.http2Port, config.host, function(){
  var addr = server.address();
  console.log(moment().format(format), 'HTTP2 server listening at:', addr.address + ':' + addr.port);
});

// Create routes
var path = '/node';

app.get(path, function(req, res) {
  if (req.cookies && req.cookies.token) {
    return res.redirect('/node/home');
  } else {
    return res.redirect('/');
  }
});

app.post(path + '/login', middlewares.loadDB, login.login);

app.get(path + '/home', [middlewares.jwtcheck, middlewares.loadDB], pages.home);

app.get(path + '/changepwd', [middlewares.jwtcheck, middlewares.loadDB], pages.change);

app.post(path + '/user/changepwd', [middlewares.jwtcheck, middlewares.loadDB], user.changepwd);

app.post(path + '/user/add', [middlewares.jwtcheck, middlewares.loadDB], user.add);

app.post(path + '/class/add', [middlewares.jwtcheck, middlewares.loadDB], classes.add);

// Custom 404
app.use(function(req, res){

  res.status(404);

  // respond with html page
  if (req.accepts('html')) {
    return res.render('404', {
      layout: false,
      url: req.url
    });
  }

  // respond with json
  if (req.accepts('json')) {
    return res.send({ error: 'Not found' });
  }

  // default to plain-text. send()
  return res.type('txt').send('Not found');
});
