#/bin/bash

# Install neccesary linux files
apt update
apt install mariadb-server nginx -y

# Get nvm

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm

# Use nvm to install Node.js

nvm install v7.4.0

# Create self signed certificate
mkdir keys

cd keys/

openssl genrsa -des3 -out thedeathstreaker_me.key 1024
openssl req -new -key thedeathstreaker_me.key -out thedeathstreaker_me.csr
cp thedeathstreaker_me.key thedeathstreaker_me.key.org
openssl rsa -in thedeathstreaker_me.key.org -out thedeathstreaker_me.key; openssl x509 -req -days 365 -in thedeathstreaker_me.csr -signkey thedeathstreaker_me.key -out thedeathstreaker_me.crt;

cd ..

# Create nginx configuration

sed -i "s|<path-to-project>|$(pwd)|g" nginx.conf

cp nginx.conf /etc/nginx/

# Restart nginx service
nginx -s reload

# Create database
mysql -u root < install/CreateDBeStudent.SQL

# Update npm
npm update -g npm

# Start server
./start.sh
