var login = function () {
  var uname = $('#username').val();
  var pass = $('#password').val();;

  $.post({
    url: 'node/login',
    data: {
      'username': uname,
      'password': pass
    },
    success: function(data) {
      window.location.href = 'node/home';
    },
    error: function (err) {
      $('#err').text(err.responseJSON.error);
    }
  });
}


var addUser = function() {
  var firstName = $('#name').val();
  var lastName = $('#lastname').val();
  var role = $('#role').val();
  var street = $('#street').val();
  var post = $('#post').val();
  var city = $('#city').val();
  var errors = [];

  var nameRegex = new RegExp('^[A-ZČŠŽĆĐ][a-yčšžćđ]+( [A-ZČŠŽĆĐ][a-yčšžćđ]+)?');
  var fnRegMatch = firstName.match(nameRegex);

  if (firstName === undefined || firstName === '') {
    errors.push('Name should not be empty.');
  } else if (!fnRegMatch) {
    errors.push('First name can consist of 2 names and both must begin with capital letter.');
  }

  var lnRegMatch = lastName.match(nameRegex);

  if (lastName === undefined || lastName === '') {
    errors.push('Last name should not be empty.');
  } else if (!lnRegMatch) {
    errors.push('Last name can consist of 2 names and both must begin with capital letter.');
  }

  if (role === undefined || role === '') {
    errors.push('Role should not be empty.');
  }

  var streetRegex = new RegExp('^[A-ZČŠŽĆĐ][a-zčšžćđ]+( [A-ZČŠŽĆĐ]?[a-zčšžćđ]+)* \\d{1,3}');
  var stRegMatch = street.match(streetRegex);

  if (street === undefined || street === '') {
    errors.push('Street should not be empty.');
  } else if (!stRegMatch) {
    errors.push('Street must include atleast one word begginig with capital letter and between 1 and 3 digits.');
  }

  var postRegex = new RegExp('[1-9]\\d{3,6}');
  var poRegMatch = post.match(postRegex);

  if (post === undefined || post === '') {
    errors.push('Post should not be empty.');
  } else if (!poRegMatch) {
    errors.push('Post must have atleast four digits and it must begin with 1-9.');
  }

  var cityRegex = new RegExp('^[A-ZČŠŽĆĐ][a-yčšžćđ]+( [A-ZČŠŽĆĐ][a-yčšžćđ]+)?');
  var ciRegMatch = city.match(cityRegex);

  if (city === undefined || city === '') {
    errors.push('City should not be empty.');
  } else if (!ciRegMatch) {
    errors.push('City can be two words (must be atleast one). Both words must begin with capital letter.');
  }

  if (errors.length > 0) {
    $('#error').html(errors.join('<br />'));
  } else {
    $('#error').html('');

    $.post({
      url: '/node/user/add',
      data: {
        'firstName': fnRegMatch[0],
        'lastName': lnRegMatch[0],
        'role': role,
        'street': stRegMatch[0],
        'post': poRegMatch[0],
        'city': ciRegMatch[0]
      },
      success: function(data) {
        $('#name').val('');
        $('#lastname').val('');
        $('#role').val('');
        $('#street').val('');
        $('#post').val('');
        $('#city').val('');

        if (role === 'professor') {
          $('#professor').append(new Option(data.user.firstName + ' ' + data.user.lastName, data.user.userID));
        }

        $('#error').html('User successfully added.');
      },
      error: function (err) {
        $('#error').html(err.responseJSON.error);
      }
    });
  }

}

var addClass = function() {
  var className = $('#clsname').val();
  var profId = $('#porfessor').val();

  if (className === undefined || className === '') {
    $('#errorcls').text('Class name should not be empty');
  } else {
    $.post({
      url: '/node/class/add',
      data: {
        'className': className,
        'profId': profId
      },
      success: function(data) {
        $('#clsname').val('');
        $('#professor').val('');

        $('#error').html('Class successfully added.');
      },
      error: function (err) {
        $('#error').html(err.responseJSON.error);
      }
    })
  }
}

var loadStudentRequest = function (id) {
  window.localStorage.setItem('request', id);
}

var checkFiles = function () {
  if (document.forms['requestForm']['files'].files.length > 2) {
    alert('Too many files. Please reupload.');
    document.forms['requestForm']['files'].value = '';
  }
}

var submit = function () {
  var requestID = parseInt(window.localStorage.getItem('request'));
  var addComments = document.forms['requestForm']['comments'].value;
  var addFiles = document.forms['requestForm']['files'].files;

  studentRequests.push({
    'idRequest': requestID,
    'idStudent': user.id,
    'comments': addComments,
    'files': addFiles.length ? addFiles : undefined
  });
  window.localStorage.removeItem('request');
  window.location.href = 'requests.html';
}

var logout = function () {
  $.removeCookie('token', {path: '/'});
  window.location.href = '/';
}

var changePwd = function () {
  var password1 = $('#password1').val();
  var password2 = $('#password2').val();

  if(password1.length < 8) {
    $('#error').text('Password too short');
  } else if (password1 === password2) {
    $.post({
      url: '/node/user/changepwd',
      data: {
        'pass1': password1,
        'pass2': password2
      },
      success: function(data) {
        window.location.href = '/node/home';
      },
      error: function (err) {
        $('#err').text(err.responseJSON.error);
      },
    });
  } else {
    $('#error').text('Passwords do not match.');
  }
}
