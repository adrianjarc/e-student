#/bin/bash

git pull

npm install

# Create documentation

./documentation.sh

# End documentation

# Minify scripts and styles

./minify.sh

# End minify

echo "Server started. You can see logs in server.log and server.error files"

sed -i "s|'ca'|// 'ca'|g" server.js

node server.js >>server.log 2>>server.error &
