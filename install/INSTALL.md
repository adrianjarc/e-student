# Starting server #
Run command ```sudo ./install.sh``` in root directory of this project. This command will install all the necessary dependencies (except Ubuntu) for this api and it will start the server. It must be run as sudo because it updates your repositories and uses apt to instal NGINX and MariaDB.

## Prerequsites ##
1. [Ubuntu 16.04 LTS](https://www.ubuntu.com/download/desktop) or [Ubuntu 16.10](https://www.ubuntu.com/download/desktop)
2. [Node.js](https://nodejs.org/en/) (API was tested on Node.js v7.2.0 and v7.4.0) for easier version control of Node.js I recommend using [nvm](https://github.com/creationix/nvm)
3. [NGINX](https://www.nginx.com/) ```sudo apt update; sudo apt install nginx;```
4. [MariaDB](https://mariadb.org/) ```sudo apt update; sudo apt install mariadb-server mariadb-server-core-10.0```

## Setting up ##
1. Go into root project folder and create folder keys (command ```mkdir keys```)
2. Inside of folder keys run following commands
```openssl genrsa -des3 -out thedeathstreaker_me.key 1024; openssl req -new -key thedeathstreaker_me.key -out thedeathstreaker_me.csr; cp thedeathstreaker_me.key thedeathstreaker_me.key.org; openssl rsa -in thedeathstreaker_me.key.org -out thedeathstreaker_me.key; openssl x509 -req -days 365 -in thedeathstreaker_me.csr -signkey thedeathstreaker_me.key -out thedeathstreaker_me.crt;
```
this should create self signed certificate needed to run this server.
3. Create user in MariaDB with username 'nodejs' and password 'adr14nc00l' you also need to create database eStudent (for easier creation of database you can use CreateDBeStudent.SQL file ```mysql -u root < CreateDBeStudent.SQL```) and give user nodejs edit rights to this database.
4. Go to root folder and change <path-to-project> in nginx.conf to absolute path on which you cloned this project.
5. Copy nginx.conf file to where your nginx configuration file is located (usually /etc/nginx/)
6. Run ```sudo nginx -S reload``` this should make nginx accept new configuration file
7. Run ```./start.sh``` this should set up nodejs API.
8. You can access this server at [https://localhost:8000/](https://localhost:8000/) and documentation at [https://localhost:8000/documentation/](https://localhost:8000/documentation/)

## Applying updates ##
If you pushed any new commits to repository just run ```./restart.sh``` on your server. It will automatically pull any new changes and restart the server.

[//]: # (Login data:)
[//]: # (Student: {Username: adrianj, Password: adrian1234})
[//]: # (Professor: {Username: janezn, Password: novak4321})
[//]: # (Referat: {Username: metak, Password: metkriz12})
