#/bin/bash

sed -i 's/localhost:8000/thedeathstreaker.me/g' apidoc.json

node_modules/apidoc/bin/apidoc -f ".*\\.js" -i controllers/ -o documentation/ -t template/

# Reposition documentation styles
mv documentation/css/* public/documentation/css/
rm -rf documentation/css/

# Reposition documentation images
mv documentation/img/* public/documentation/img/
rm -rf documentation/img/

# Reposition documentation locales
mv documentation/locales/* public/documentation/locales/
rm -rf documentation/locales/

# Reposition documentation utils
mv documentation/utils/* public/documentation/utils/
rm -rf documentation/utils/

# Reposition documentation vendor
mv documentation/vendor/path-to-regexp/* public/documentation/vendor/path-to-regexp/
rm -rf documentation/vendor/path-to-regexp/

mv documentation/vendor/prettify/* public/documentation/vendor/prettify/
rm -rf documentation/vendor/prettify/

mv documentation/vendor/* public/documentation/vendor/
rm -rf documentation/vendor/

# Reposition remainig files
mv documentation/*.js public/documentation/
mv documentation/*.json public/documentation/
