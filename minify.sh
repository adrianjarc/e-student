#/bin/bash

# Run minification file

node minify.js


# Replace script.js with script.min.js in neccesary files
sed -i 's/script.js/script.min.js/g' views/layouts/main.hbs
sed -i 's/script.js/script.min.js/g' views/index.html


# Replace style.css with style.min.css in neccesary files
sed -i 's/style.css/style.min.css/g' views/layouts/main.hbs
sed -i 's/style.css/style.min.css/g' views/index.html
