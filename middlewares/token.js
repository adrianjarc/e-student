/**
  * This middleware check if token exists in request from web app.
  * If token does exist everything proceeds as normal and user id is save into request.
  * If token doesnt exist it throws error 403 and logs user out.
  */

var jwt = require('jsonwebtoken'),
    moment = require('moment');

var config = require(__dirname + '/../config.json');

module.exports = function (req, res, next) {
  // check header or url parameters or post parameters for token
  var token = req.body.token || req.query.token || req.headers['x-access-token'] || req.cookies.token;

  // decode token
  if (token) {

    // verifies secret and checks exp
    jwt.verify(token, config.secret, function(err, decoded) {
      if (err) {
        return res.status(403).redirect('/');
      } else {
        // if everything is good, save to request for use in other routes
        req.user = decoded;
        req.format = 'D.M.YYYY HH:mm:';
        next();
      }
    });

  } else {

    // if there is no token
    // return an error
    return res.status(403).redirect('/');

  }
};
