/**
  * This middleware loads all database models to req.db
  * so all database tables can be reached from any controller.
  */

module.exports = function (req, res, next) {
  if (!req.db) {
    req.db = {
      users: require(__dirname + '/../models/user.js'),
      classes: require(__dirname + '/../models/classes.js'),
      hascls: require(__dirname + '/../models/hasclass.js'),
      exams: require(__dirname + '/../models/exams.js'),
      orders: require(__dirname + '/../models/orders.js'),
      requests: require(__dirname + '/../models/requests.js'),
      studOrders: require(__dirname + '/../models/studentorders.js'),
      studRequests: require(__dirname + '/../models/studentrequests.js'),
      files: require(__dirname + '/../models/files.js'),
    }
  }
  next();
};
