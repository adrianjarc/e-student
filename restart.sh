#/bin/bash


beforePackage=$(date -r package.json)

git fetch
git stash
git rebase origin/master
git stash pop

afterPackage=$(date -r package.json)

# Create documentation

./documentation.sh

# End documentation


# Minify scripts and styles

./minify.sh

# End minify


# Only run npm install if package.json was changed
if [ "$beforePackage" != "$afterPackage" ]; then
  npm install
fi

# Kill currently running server and create new one

a=$(netstat -ap --numeric-ports | grep :8080)

b=$(echo $a | cut -d' ' -f 7)

c=$(echo $b | cut -d'/' -f 1)

kill -9 $c

echo "Server restarted. You can see logs in server.log and server.error files"

sed -i "s|'ca'|// 'ca'|g" server.js

node server.js >>server.log 2>>server.error &
