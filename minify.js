var compressor = require('node-minify');

compressor.minify({
  compressor: 'uglifyjs',
  input: './public/javascript/script.js',
   output: './public/javascript/script.min.js',
  callback: function (err, min) {}
});

compressor.minify({
  compressor: 'csso',
  input: './public/css/style.css',
   output: './public/css/style.min.css',
  options: {
    restructureOff: true // turns structure minimization off
  },
  callback: function (err, min) {}
});
